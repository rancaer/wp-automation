import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Need to edit: ticket url'
ticketUrl = 'https://issues.amazon.com/issues/WP-5875'

groupUrl = 'https://permissions.amazon.com/group.mhtml?target=10589069'

WebUI.openBrowser(ticketUrl)

WebUI.waitForElementVisible(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/span_User Alias'), 0)

userAlias = WebUI.getText(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/span_User Alias'))

WebUI.navigateToUrl(groupUrl)

WebUI.waitForElementVisible(findTestObject('CMS Permission New User/Page_Account Management Group/input_User Alias'), 0)

WebUI.click(findTestObject('CMS Permission New User/Page_Account Management Group/input_User Alias'))

WebUI.sendKeys(findTestObject('CMS Permission New User/Page_Account Management Group/input_User Alias'), userAlias)

WebUI.click(findTestObject('CMS Permission New User/Page_Account Management Group/button_Add Member'))

WebUI.waitForElementVisible(findTestObject('CMS Permission New User/Page_Account Management Group/section_Change'), 0)

WebUI.waitForElementVisible(findTestObject('CMS Permission New User/Page_Account Management Group/font_Add access'), 0)

WebUI.waitForElementVisible(findTestObject('CMS Permission New User/Page_Account Management Group/font_APPROVED'), 0)

WebUI.navigateToUrl(ticketUrl)

WebUI.waitForElementClickable(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/button_Markdown'), 0)

WebUI.click(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/button_Markdown'))

WebUI.click(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/textarea_Comment'))

WebUI.sendKeys(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/textarea_Comment'), 'Thank you for requesting CMS access. You now have read access to our staging/production environment and read/write access to the content-sandbox.\n\nBefore you can start editing pages in staging, you\'ll need to complete the mini tutorial linked to at the top of this ticket. \n\n**Once the tutorial is complete, you need to comment here with your CMS and preview links so that we can check your work and finish configuring your permissions and award you the phone tool icon. Also, be sure to tag us and change the “Next Step” in SIM to “Tutorial Complete”. We will not know you completed the tutorial if you do not tag us! **\n\n*Logging In*\n\nTo log in to the CMS, please visit https://aws-cms.aka.amazon.com and enter your Amazon user name and password (making sure you leave off @amazon.com). Your password is synced via LDAP so anytime you need to update your password, do it at https://password-v2.corp.amazon.com/. If you change it anywhere else you won\'t be able to log in to the CMS.\n')

WebUI.waitForElementVisible(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/select_Set Step'), 0)

WebUI.waitForElementClickable(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/select_Set Step'), 0)

WebUI.sendKeys(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/select_Set Step'), 'Begin Tutorial,role:requester')

WebUI.waitForElementClickable(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/button_Comment'), 0)

WebUI.click(findTestObject('CMS Permission New User/Page_CMS Permissions Ticket/button_Comment'))

